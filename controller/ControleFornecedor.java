package controller;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

public class ControleFornecedor {

    private final ArrayList<PessoaJuridica> listaPessoasJuridica;
    private final ArrayList<PessoaFisica> listaPessoasFisica;
    
    public ControleFornecedor(){
        listaPessoasJuridica = new ArrayList<PessoaJuridica>(); 
        listaPessoasFisica = new ArrayList<PessoaFisica>();
    }
    
    public ArrayList<PessoaJuridica> getListaPessoasJuridica(){
        return listaPessoasJuridica;
    }
    public ArrayList<PessoaFisica> getListaPessoasFisica(){
        return listaPessoasFisica;
    }
    
    public void adicionar(PessoaFisica fornecedor){
        listaPessoasFisica.add(fornecedor);
    }
    
    public void adicionar(PessoaJuridica fornecedor){
        listaPessoasJuridica.add(fornecedor);
    }
    
    public void removerFisica(PessoaFisica umaPessoa){
        listaPessoasFisica.remove(umaPessoa);
    }
    
    public void removerJuridica(PessoaJuridica umaPessoa){
        listaPessoasJuridica.remove(umaPessoa);
    }
    
    public PessoaFisica procurarPessoaFisica(String umNome){
        for(PessoaFisica umaPessoaFisica : listaPessoasFisica ){
            if (umaPessoaFisica.getNome().equalsIgnoreCase(umNome)) 
                return umaPessoaFisica;
        }
        return null;
    }
    public PessoaJuridica procurarPessoaJuridica(String umNome){
        for(PessoaJuridica umaPessoaJuridica : listaPessoasJuridica ){
            if (umaPessoaJuridica.getNome().equalsIgnoreCase(umNome)) 
                return umaPessoaJuridica;
        }
        return null;
    }
}
