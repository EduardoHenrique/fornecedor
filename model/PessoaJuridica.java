package model;

import java.util.ArrayList;

public class PessoaJuridica extends Fornecedor{
    
    public PessoaJuridica (String nome, String telefone, Produto umProduto, 
                        Endereco endereco, String cnpj, String razaoSocial){
        super(nome,telefone,umProduto,endereco);
        this.cnpj=cnpj;
        this.razaoSocial=razaoSocial;
    }
    
    private String cnpj;
    private String razaoSocial;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public void setTelefone2(String telefone2){
        super.setTelefone2(telefone2);
    }
    /*public ArrayList<Produto> getListaProdutos(){
        return produtos;
    }*/
}