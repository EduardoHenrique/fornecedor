package model;

import java.util.ArrayList;

public class Fornecedor {

    private String nome;
    private String telefone1;
    private String telefone2;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    
    public Fornecedor (String nome, String telefone1, Produto umProduto, Endereco endereco){
        this.nome=nome;
        this.telefone1=telefone1;
        produtos = new ArrayList<Produto>();
        produtos.add(umProduto);
        this.endereco = endereco;
    }
    
    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void adicionarProduto(Produto umProduto){
        produtos.add(umProduto);
    }
    
    public void removerProduto(Produto umProduto){
        produtos.remove(umProduto);
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }
    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }
    
}
