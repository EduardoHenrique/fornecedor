package model;

import java.util.ArrayList;

public class PessoaFisica extends Fornecedor{

    private String cpf;
    
    public PessoaFisica (String nome, String telefone, Produto umProduto, 
                            Endereco endereco, String cpf){
        super(nome,telefone,umProduto,endereco);
        this.cpf=cpf;
    }
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public void setTelefone2(String telefone2){
        super.setTelefone2(telefone2);
    }
    /*public ArrayList<Produto> getListaProdutos(){
        return produtos;
    }*/
}