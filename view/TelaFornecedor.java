package view;

import controller.ControleFornecedor;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Endereco;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;

public class TelaFornecedor extends javax.swing.JFrame {    
    
    ControleFornecedor umControleFornecedor = new ControleFornecedor();
    Fornecedor umFornecedor;
    PessoaJuridica umaPessoaJuridica;
    PessoaFisica umaPessoaFisica;
    Endereco umEndereco;
    Produto umProduto;
    private boolean modo;
    private boolean novoFornecedor;
    private boolean novoProduto;
    private int tipopessoa=0;
    private int choice;
    
    public TelaFornecedor() {
        initComponents();
        modo = false;
        habilitacaoCampos();
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonPesquisar.setEnabled(false);
        jButtonSalvar.setEnabled(false);
        jButtonCancelar.setEnabled(false);
        jTableProdutos.setVisible(false);
        jButtonSalvarProduto.setVisible(false);
        jButtonSalvarProduto.setEnabled(false);
        jButtonExcluirProduto.setVisible(false);
        jButtonExcluirProduto.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonNovo = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jLabelTipoPessoa = new javax.swing.JLabel();
        jComboBoxTipoPessoa = new javax.swing.JComboBox();
        jLabelNome = new javax.swing.JLabel();
        jLabelTelefone1 = new javax.swing.JLabel();
        jTextFieldTelefone1 = new javax.swing.JTextField();
        jLabelTelefone2 = new javax.swing.JLabel();
        jTextFieldTelefone2 = new javax.swing.JTextField();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelCnpj = new javax.swing.JLabel();
        jLabelRazaoSocial = new javax.swing.JLabel();
        jTextFieldCnpj = new javax.swing.JTextField();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jLabelEndereco = new javax.swing.JLabel();
        jLabelPais = new javax.swing.JLabel();
        jLabelEstado = new javax.swing.JLabel();
        jLabelCidade = new javax.swing.JLabel();
        jLabelBairro = new javax.swing.JLabel();
        jLabelLogradouro = new javax.swing.JLabel();
        jLabelComplemento = new javax.swing.JLabel();
        jTextFieldPais = new javax.swing.JTextField();
        jTextFieldEstado = new javax.swing.JTextField();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldComplemento = new javax.swing.JTextField();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jLabelInformacoesGerais = new javax.swing.JLabel();
        jLabelNumero = new javax.swing.JLabel();
        jLabelProdutos = new javax.swing.JLabel();
        jButtonExcluir = new javax.swing.JButton();
        jLabelNomeProduto = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabelDescricao = new javax.swing.JLabel();
        jTextFieldDescricao = new javax.swing.JTextField();
        jLabelValorCompra = new javax.swing.JLabel();
        jLabelValorVenda = new javax.swing.JLabel();
        jLabelQuantidade = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jButtonSalvarProduto = new javax.swing.JButton();
        jTextFieldNumero = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableProdutos = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableFornecedores = new javax.swing.JTable();
        jTextFieldBairro = new javax.swing.JTextField();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jButtonPesquisar = new javax.swing.JButton();
        jTextFieldCpf = new javax.swing.JTextField();
        jButtonExcluirProduto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jLabelTipoPessoa.setText("Tipo Pessoa:");

        jComboBoxTipoPessoa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pessoa Física", "Pessoa Jurídica" }));
        jComboBoxTipoPessoa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTipoPessoaItemStateChanged(evt);
            }
        });

        jLabelNome.setText("Nome:");

        jLabelTelefone1.setText("Telefone 1:");

        jLabelTelefone2.setText("Telefone 2:");

        jLabelCpf.setText("CPF:");

        jLabelCnpj.setText("CNPJ:");

        jLabelRazaoSocial.setText("Razão Social:");

        jLabelEndereco.setText("Endereço");

        jLabelPais.setText("País:");

        jLabelEstado.setText("Estado:");

        jLabelCidade.setText("Cidade:");

        jLabelBairro.setText("Bairro:");

        jLabelLogradouro.setText("Logradouro:");

        jLabelComplemento.setText("Complemento:");

        jLabelInformacoesGerais.setText("Informações Gerais");

        jLabelNumero.setText("Nº");

        jLabelProdutos.setText("Produtos");

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jLabelNomeProduto.setText("Nome:");

        jLabelDescricao.setText("Descrição:");

        jLabelValorCompra.setText("Valor Compra:");

        jLabelValorVenda.setText("Valor Venda:");

        jLabelQuantidade.setText("Quantidade:");

        jButtonSalvarProduto.setText("Salvar Produto");
        jButtonSalvarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarProdutoActionPerformed(evt);
            }
        });

        jTableProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Produto", "Valor Compra", "Valor Venda", "Quantidade"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProdutosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableProdutos);

        jTableFornecedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nome", "Telefone 1", "Telefone 2", "CPF/CNPJ"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableFornecedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableFornecedoresMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableFornecedores);

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonExcluirProduto.setText("Excluir Produto");
        jButtonExcluirProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelPais)
                                    .addComponent(jLabelCidade)
                                    .addComponent(jLabelNome)
                                    .addComponent(jLabelTelefone1)
                                    .addComponent(jLabelTelefone2)
                                    .addComponent(jLabelLogradouro)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelNomeProduto)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabelCnpj)
                                                    .addComponent(jLabelRazaoSocial)
                                                    .addComponent(jLabelCpf)
                                                    .addComponent(jLabelEstado)
                                                    .addComponent(jLabelBairro)
                                                    .addComponent(jLabelComplemento)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jTextFieldPais, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jTextFieldTelefone1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jTextFieldTelefone2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(145, 145, 145)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(20, 20, 20)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jTextFieldCidade)
                                                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.Alignment.LEADING)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(jTextFieldCnpj)
                                                    .addComponent(jTextFieldCpf)
                                                    .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(jLabelValorCompra)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabelValorVenda)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(204, 204, 204)
                                                .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabelQuantidade)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabelDescricao)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jLabelNumero)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                                .addGap(6, 6, 6))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabelProdutos, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelInformacoesGerais, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelEndereco, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jButtonSalvarProduto)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButtonExcluirProduto)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(70, 70, 70))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonNovo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonEditar)
                                .addGap(7, 7, 7)
                                .addComponent(jButtonPesquisar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonExcluir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonSalvar)
                                .addGap(30, 30, 30)
                                .addComponent(jButtonCancelar)))
                        .addGap(76, 76, 76))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonNovo)
                    .addComponent(jButtonEditar)
                    .addComponent(jButtonExcluir)
                    .addComponent(jButtonSalvar)
                    .addComponent(jButtonCancelar)
                    .addComponent(jButtonPesquisar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTipoPessoa)
                    .addComponent(jComboBoxTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabelInformacoesGerais)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCpf)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTelefone1)
                    .addComponent(jTextFieldTelefone1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCnpj)
                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTelefone2)
                    .addComponent(jTextFieldTelefone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelRazaoSocial)
                    .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(jLabelEndereco)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPais)
                    .addComponent(jTextFieldEstado)
                    .addComponent(jLabelEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCidade)
                    .addComponent(jLabelBairro)
                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelComplemento)
                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelNumero)
                    .addComponent(jLabelLogradouro)
                    .addComponent(jTextFieldLogradouro))
                .addGap(32, 32, 32)
                .addComponent(jLabelProdutos)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNomeProduto)
                    .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelValorCompra)
                    .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelValorVenda)
                    .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDescricao)
                    .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelQuantidade)
                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonExcluirProduto)
                    .addComponent(jButtonSalvarProduto))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
    modo = true;
    novoFornecedor = true;
    novoProduto = true;
    tipopessoa = 0;
    habilitacaoCampos();
    limparCampos();
    jComboBoxTipoPessoa.setSelectedIndex(0);
    jTextFieldNome.requestFocus();
    jButtonNovo.setEnabled(false);
    jButtonPesquisar.setEnabled(false);
    jButtonSalvar.setEnabled(true);
    jButtonCancelar.setEnabled(true);
    jButtonEditar.setEnabled(false);
    jTextFieldCnpj.setEnabled(false);
    jTextFieldRazaoSocial.setEnabled(false);
    jTableProdutos.setVisible(false);
    jButtonSalvarProduto.setVisible(false);
    jButtonExcluirProduto.setVisible(false);
}//GEN-LAST:event_jButtonNovoActionPerformed

private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
    modo=true;
    habilitacaoCampos();
    novoProduto=true;
    jTextFieldNome.requestFocus();
    jButtonSalvar.setEnabled(true);
    jButtonCancelar.setEnabled(true);
    jButtonEditar.setEnabled(false);
    jButtonNovo.setEnabled(false);
    jButtonPesquisar.setEnabled(false);
    jButtonSalvarProduto.setVisible(true);
    jButtonSalvarProduto.setEnabled(true);
    jButtonExcluirProduto.setVisible(true);
    jButtonExcluirProduto.setEnabled(true);
    jTableProdutos.setEnabled(true);
    
    if(tipopessoa==0){
        jTextFieldCnpj.setEnabled(false);
        jTextFieldRazaoSocial.setEnabled(false);
    }
    else if(tipopessoa==1)
        jTextFieldCpf.setEnabled(false);
}//GEN-LAST:event_jButtonEditarActionPerformed

private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
    if(tipopessoa==0)
        umControleFornecedor.removerFisica(umaPessoaFisica);
    else if(tipopessoa==1)
        umControleFornecedor.removerJuridica(umaPessoaJuridica);
    
    jButtonEditar.setEnabled(false);
    jButtonExcluir.setEnabled(false);
    jButtonNovo.setEnabled(true);
    jButtonPesquisar.setEnabled(true);
    jButtonSalvar.setEnabled(false);
    jButtonCancelar.setEnabled(false);
    
    carregarListaFornecedores();
}//GEN-LAST:event_jButtonExcluirActionPerformed

private void jButtonSalvarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarProdutoActionPerformed

    String umNomeProduto = jTextFieldNomeProduto.getText();
    String umaDescricao = jTextFieldDescricao.getText();
    double umValorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
    double umValorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
    double umaQuantidade = Double.parseDouble(jTextFieldQuantidade.getText());

    if(novoProduto==true){
        umProduto = new Produto(umNomeProduto, umaDescricao, umValorCompra, umValorVenda, umaQuantidade);
        
        if(tipopessoa==0)
            umaPessoaFisica.adicionarProduto(umProduto);
        else if(tipopessoa==1)
            umaPessoaJuridica.adicionarProduto(umProduto);
    }
    
    else if(novoProduto==false){
        umProduto.setNomeProduto(umNomeProduto);
        umProduto.setValorCompra(umValorCompra);
        umProduto.setValorVenda(umValorVenda);
        umProduto.setQuantidade(umaQuantidade);
        umProduto.setDescricao(umaDescricao);
    }

    carregarListaProdutos();
    
    limparCamposProduto();
    
    novoProduto=true;
    
}//GEN-LAST:event_jButtonSalvarProdutoActionPerformed

private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
    modo = false;
    habilitacaoCampos();
    limparCampos();
    jButtonNovo.setEnabled(true);
    jButtonPesquisar.setEnabled(true);
    jButtonEditar.setEnabled(false);
    jButtonSalvar.setEnabled(false);
    jButtonCancelar.setEnabled(false);
    jButtonSalvarProduto.setEnabled(false);
}//GEN-LAST:event_jButtonCancelarActionPerformed

private void jComboBoxTipoPessoaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTipoPessoaItemStateChanged
    if (jComboBoxTipoPessoa.getSelectedItem().toString().equals("Pessoa Física")) {
        jTextFieldCnpj.setEnabled(false);
        jTextFieldCpf.setEnabled(true);
        jTextFieldRazaoSocial.setEnabled(false);
        jTextFieldCnpj.setText(null);
        jTextFieldRazaoSocial.setText(null);
        tipopessoa=0;
    } 
    else {
        this.jTextFieldCnpj.setEnabled(true);
        this.jTextFieldCpf.setEnabled(false);
        this.jTextFieldRazaoSocial.setEnabled(true);
        this.jTextFieldCpf.setText(null);
        tipopessoa=1;
    }
    carregarListaFornecedores();
}//GEN-LAST:event_jComboBoxTipoPessoaItemStateChanged

private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
    if (validarCampos() == true) 
    {
        if(tipopessoa==0) {
            umaPessoaFisica = salvarPessoaFisica();
        }
        else{
            umaPessoaJuridica = salvarPessoaJuridica();
        }
        carregarListaFornecedores();
        limparCampos();
        modo = false;
        habilitacaoCampos();
        jButtonNovo.setEnabled(true);
        jButtonEditar.setEnabled(false);
        jButtonPesquisar.setEnabled(true);
        limparListaProdutos();
        novoFornecedor = false;
        novoProduto = false;
        jButtonSalvarProduto.setEnabled(false);
        jButtonExcluirProduto.setEnabled(false);
        jButtonSalvar.setEnabled(false);
        jButtonEditar.setEnabled(false);
    }
}//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
        String nomepesquisa = JOptionPane.showInputDialog("Informe o nome da Pessoa.");
        jTableProdutos.setVisible(true);
        jButtonSalvarProduto.setVisible(true);
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        
        if(nomepesquisa!=null){
            PessoaFisica outraPessoaFisica = umControleFornecedor.procurarPessoaFisica(nomepesquisa);
            PessoaJuridica outraPessoaJuridica = umControleFornecedor.procurarPessoaJuridica(nomepesquisa);
            
            if (outraPessoaFisica == null && outraPessoaJuridica == null)
                exibirInformacao("Pessoa não encontrada!");
             
            else if (outraPessoaFisica != null) {
                this.umaPessoaFisica = outraPessoaFisica;
                choice=0;
                preencherCampos();
                carregarListaProdutos();
            }
            
            else if (outraPessoaJuridica != null) {
                this.umaPessoaJuridica = outraPessoaJuridica;
                choice=1;
                preencherCampos();
                carregarListaProdutos();
            }
        }
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jTableFornecedoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableFornecedoresMouseClicked
        DefaultTableModel model = (DefaultTableModel) jTableFornecedores.getModel();
        String nomepesquisa = (String) model.getValueAt(jTableFornecedores.getSelectedRow(), 0);
        if(tipopessoa==0){
            umaPessoaFisica=umControleFornecedor.procurarPessoaFisica(nomepesquisa);
            choice=0;
        }
        
        else if(tipopessoa==1){
            umaPessoaJuridica = umControleFornecedor.procurarPessoaJuridica(nomepesquisa);
            choice=1;
        }
        jTableProdutos.setVisible(true);
        jTableProdutos.setEnabled(false);
        preencherCampos();
        carregarListaProdutos();
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
    }//GEN-LAST:event_jTableFornecedoresMouseClicked

    private void jTableProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProdutosMouseClicked
        DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
        String nomepesquisa = (String) model.getValueAt(jTableProdutos.getSelectedRow(), 0);
        if (tipopessoa==0){
            ArrayList<Produto> listaProdutos = umaPessoaFisica.getProdutos();
            for(Produto p : listaProdutos){
                if(p.getNomeProduto().equals(nomepesquisa)){
                    umProduto = p;
                    preencherCamposProduto();
                }
            }
        }
        else if(tipopessoa==1){
            ArrayList<Produto> listaProdutos = umaPessoaJuridica.getProdutos();
            for(Produto p : listaProdutos){
                if(p.getNomeProduto().equalsIgnoreCase(nomepesquisa)){
                    umProduto = p;
                    preencherCamposProduto();
                }
            }
        }
        novoProduto=false;
    }//GEN-LAST:event_jTableProdutosMouseClicked

    private void jButtonExcluirProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirProdutoActionPerformed
        
        if(tipopessoa==0)
            umaPessoaFisica.removerProduto(umProduto);
        
        else if(tipopessoa==1)
            umaPessoaJuridica.removerProduto(umProduto);
        
        carregarListaProdutos();
        limparCamposProduto();
        novoProduto=true;
        
    }//GEN-LAST:event_jButtonExcluirProdutoActionPerformed

    public PessoaFisica salvarPessoaFisica(){
        String umNome = jTextFieldNome.getText();
        String umCpf = jTextFieldCpf.getText();
        String umTelefone = jTextFieldTelefone1.getText();

        String umLogradouro=jTextFieldLogradouro.getText();
        String umNumero=jTextFieldNumero.getText();
        String umBairro=jTextFieldBairro.getText();
        String umaCidade=jTextFieldCidade.getText();
        String umEstado=jTextFieldEstado.getText();
        String umPais=jTextFieldPais.getText();
        String umComplemento=jTextFieldComplemento.getText();
        
        String umNomeProduto = jTextFieldNomeProduto.getText();
        String umaDescricao = jTextFieldDescricao.getText();
        double umValorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
        double umValorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
        double umaQuantidade = Double.parseDouble(jTextFieldQuantidade.getText());
        
        if (novoFornecedor == true) {
            
            umProduto = new Produto(umNomeProduto, umaDescricao, umValorCompra, umValorVenda, umaQuantidade);                  
            umEndereco = new Endereco(umLogradouro,umNumero,umBairro,umaCidade,umEstado,umPais,umComplemento);           
            umaPessoaFisica = new PessoaFisica(umNome,umTelefone,umProduto,umEndereco,umCpf);           
            umaPessoaFisica.setTelefone2(jTextFieldTelefone2.getText());
            umControleFornecedor.adicionar(umaPessoaFisica);
            
        } 
        
        else {
            umaPessoaFisica.setNome(umNome);
            umaPessoaFisica.setTelefone1(umTelefone);
            umaPessoaFisica.setTelefone2(jTextFieldTelefone2.getText());
            umaPessoaFisica.setCpf(umCpf);
            
            umEndereco = new Endereco(umLogradouro,umNumero,umBairro,umaCidade,umEstado,umPais,umComplemento);
            umaPessoaFisica.setEndereco(umEndereco);
        }

        return umaPessoaFisica;
    }
    
    public PessoaJuridica salvarPessoaJuridica(){
        
        String umNome = jTextFieldNome.getText();
        String umCnpj = jTextFieldCnpj.getText();
        String umaRazaoSocial = jTextFieldRazaoSocial.getText();
        String umTelefone = jTextFieldTelefone1.getText();

        String umLogradouro=jTextFieldLogradouro.getText();
        String umNumero=jTextFieldNumero.getText();
        String umBairro=jTextFieldBairro.getText();
        String umaCidade=jTextFieldCidade.getText();
        String umEstado=jTextFieldEstado.getText();
        String umPais=jTextFieldPais.getText();
        String umComplemento=jTextFieldComplemento.getText();
        
        String umNomeProduto = jTextFieldNomeProduto.getText();
        String umaDescricao = jTextFieldDescricao.getText();
        double umValorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
        double umValorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
        double umaQuantidade = Double.parseDouble(jTextFieldQuantidade.getText());
        
       if (novoFornecedor == true) {
           
            umProduto = new Produto(umNomeProduto, umaDescricao, umValorCompra, umValorVenda, umaQuantidade);
            umEndereco = new Endereco(umLogradouro,umNumero,umBairro,umaCidade,umEstado,umPais,umComplemento);            
            umaPessoaJuridica = new PessoaJuridica(umNome,umTelefone,umProduto,umEndereco,umCnpj,umaRazaoSocial);            
            umaPessoaJuridica.setTelefone2(jTextFieldTelefone2.getText());           
            umControleFornecedor.adicionar(umaPessoaJuridica);
        }
       
        else {
           
            umaPessoaJuridica.setNome(umNome);
            umaPessoaJuridica.setTelefone1(umTelefone);
            umaPessoaJuridica.setTelefone2(jTextFieldTelefone2.getText());
            umaPessoaJuridica.setCnpj(umCnpj);
            umaPessoaJuridica.setRazaoSocial(umaRazaoSocial);

            umEndereco = new Endereco(umLogradouro,umNumero,umBairro,umaCidade,umEstado,umPais,umComplemento);
            umaPessoaJuridica.setEndereco(umEndereco);
        }

        return umaPessoaJuridica; 
    }
    
    public void carregarListaFornecedores(){
        if (tipopessoa==0){
            ArrayList<PessoaFisica> listaPessoas = umControleFornecedor.getListaPessoasFisica();
            DefaultTableModel model = (DefaultTableModel) jTableFornecedores.getModel();
            model.setRowCount(0);
            for (PessoaFisica f : listaPessoas) {
                model.addRow(new String[]{f.getNome(), f.getTelefone1(), f.getTelefone2(), f.getCpf()});
            }
            jTableFornecedores.setModel(model);
        }
        else if(tipopessoa==1){
            ArrayList<PessoaJuridica> listaPessoas = umControleFornecedor.getListaPessoasJuridica();
            DefaultTableModel model = (DefaultTableModel) jTableFornecedores.getModel();
            model.setRowCount(0);
            for (PessoaJuridica f : listaPessoas) {
                model.addRow(new String[]{f.getNome(), f.getTelefone1(), f.getTelefone2(), f.getCnpj()});
            }
            jTableFornecedores.setModel(model);
        }
    }

    public void carregarListaProdutos(){
        if(tipopessoa==0){
            ArrayList<Produto> listaProdutos = umaPessoaFisica.getProdutos();
            DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
            model.setRowCount(0);
            for (Produto p : listaProdutos){
                String quant = Double.toString(p.getQuantidade());
                String compra = Double.toString(p.getValorCompra());
                String venda = Double.toString(p.getValorVenda());
                model.addRow(new String[]{p.getNomeProduto(), compra, venda, quant});
            }
            jTableProdutos.setModel(model);
        }
        else{
            ArrayList<Produto> listaProdutos = umaPessoaJuridica.getProdutos();
            DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
            model.setRowCount(0);
            for (Produto p : listaProdutos){
                String quant = Double.toString(p.getQuantidade());
                String compra = Double.toString(p.getValorCompra());
                String venda = Double.toString(p.getValorVenda());
                model.addRow(new String[]{p.getNomeProduto(), quant, compra, venda, quant});
            }
            jTableProdutos.setModel(model);
        }
    }
    
    public void limparListaProdutos(){
        DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
        model.setRowCount(0);
        jTableProdutos.setModel(model);
    }
    
    private void preencherCampos(){
        if(choice==0){
            jTextFieldNome.setText(umaPessoaFisica.getNome());
            jTextFieldTelefone1.setText(umaPessoaFisica.getTelefone1());
            jTextFieldTelefone2.setText(umaPessoaFisica.getTelefone2());
            jTextFieldCpf.setText(umaPessoaFisica.getCpf());
            jTextFieldCnpj.setText(null);
            jTextFieldRazaoSocial.setText(null);
            jTextFieldPais.setText(umaPessoaFisica.getEndereco().getPais());
            jTextFieldEstado.setText(umaPessoaFisica.getEndereco().getEstado());
            jTextFieldBairro.setText(umaPessoaFisica.getEndereco().getBairro());
            jTextFieldCidade.setText(umaPessoaFisica.getEndereco().getCidade());
            jTextFieldComplemento.setText(umaPessoaFisica.getEndereco().getComplemento());
            jTextFieldLogradouro.setText(umaPessoaFisica.getEndereco().getLogradouro());
            jTextFieldNumero.setText(umaPessoaFisica.getEndereco().getNumero());
        }
        else if(choice==1){
            jTextFieldNome.setText(umaPessoaJuridica.getNome());
            jTextFieldBairro.setText(umaPessoaJuridica.getEndereco().getBairro());
            jTextFieldCidade.setText(umaPessoaJuridica.getEndereco().getCidade());
            jTextFieldCnpj.setText(umaPessoaJuridica.getCnpj());
            jTextFieldRazaoSocial.setText(umaPessoaJuridica.getRazaoSocial());
            jTextFieldComplemento.setText(umaPessoaJuridica.getEndereco().getComplemento());
            jTextFieldCpf.setText(null);
            jTextFieldLogradouro.setText(umaPessoaJuridica.getEndereco().getLogradouro());
            jTextFieldNumero.setText(umaPessoaJuridica.getEndereco().getNumero());
            jTextFieldPais.setText(umaPessoaJuridica.getEndereco().getPais());
            jTextFieldEstado.setText(umaPessoaJuridica.getEndereco().getEstado());
            jTextFieldTelefone1.setText(umaPessoaJuridica.getTelefone1());
            jTextFieldTelefone2.setText(umaPessoaJuridica.getTelefone2());
        }
    }
    
    public void preencherCamposProduto(){
        jTextFieldNomeProduto.setText(umProduto.getNomeProduto());
        jTextFieldValorCompra.setText(Double.toString(umProduto.getValorCompra()));
        jTextFieldValorVenda.setText(Double.toString(umProduto.getValorVenda()));
        jTextFieldQuantidade.setText(Double.toString(umProduto.getQuantidade()));
        jTextFieldDescricao.setText(umProduto.getDescricao());
    }
    
    public void habilitacaoCampos () {
        jTextFieldBairro.setEnabled(modo);
        jTextFieldCidade.setEnabled(modo);
        jTextFieldComplemento.setEnabled(modo);
        jTextFieldCpf.setEnabled(modo);
        jTextFieldDescricao.setEnabled(modo);
        jTextFieldLogradouro.setEnabled(modo);
        jTextFieldNome.setEnabled(modo);
        jTextFieldNomeProduto.setEnabled(modo);
        jTextFieldNumero.setEnabled(modo);
        jTextFieldPais.setEnabled(modo);
        jTextFieldTelefone2.setEnabled(modo);
        jTextFieldTelefone1.setEnabled(modo);
        jTextFieldCnpj.setEnabled(modo);
        jTextFieldEstado.setEnabled(modo);
        jTextFieldQuantidade.setEnabled(modo);
        jTextFieldValorCompra.setEnabled(modo);
        jTextFieldValorVenda.setEnabled(modo);
        jTextFieldRazaoSocial.setEnabled(modo);
    }
    
    public void limparCampos () {
        jTextFieldBairro.setText(null);
        jTextFieldCidade.setText(null);
        jTextFieldCnpj.setText(null);
        jTextFieldComplemento.setText(null);
        jTextFieldCpf.setText(null);
        jTextFieldDescricao.setText(null);
        jTextFieldLogradouro.setText(null);
        jTextFieldNome.setText(null);
        jTextFieldNomeProduto.setText(null);
        jTextFieldNumero.setText(null);
        jTextFieldPais.setText(null);
        jTextFieldTelefone2.setText(null);
        jTextFieldTelefone1.setText(null);
        jTextFieldValorCompra.setText("0.0");
        jTextFieldValorVenda.setText("0.0");
        jTextFieldQuantidade.setText("0.0");
        jTextFieldRazaoSocial.setText(null);
        jTextFieldEstado.setText(null);
    }
    
    public void limparCamposProduto (){
        jTextFieldNomeProduto.setText(null);
        jTextFieldDescricao.setText(null);
        jTextFieldValorCompra.setText("0.0");
        jTextFieldValorVenda.setText("0.0");
        jTextFieldQuantidade.setText("0.0");
    }
    
    private void exibirInformacao (String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean validarCampos () {
        if (jTextFieldNome.getText().trim().length() == 0) {
            exibirInformacao("Informe o 'Nome'");
            jTextFieldNome.requestFocus();
            return false;
        }
        else if (jTextFieldNomeProduto.getText().trim().length() == 0 && novoFornecedor==true){
            exibirInformacao("Digite o nome do produto");
            jTextFieldNomeProduto.requestFocus();
            return false;
        }
        else {
            return true;
        }
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new TelaFornecedor().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonExcluirProduto;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonSalvarProduto;
    private javax.swing.JComboBox jComboBoxTipoPessoa;
    private javax.swing.JLabel jLabelBairro;
    private javax.swing.JLabel jLabelCidade;
    private javax.swing.JLabel jLabelCnpj;
    private javax.swing.JLabel jLabelComplemento;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelDescricao;
    private javax.swing.JLabel jLabelEndereco;
    private javax.swing.JLabel jLabelEstado;
    private javax.swing.JLabel jLabelInformacoesGerais;
    private javax.swing.JLabel jLabelLogradouro;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNomeProduto;
    private javax.swing.JLabel jLabelNumero;
    private javax.swing.JLabel jLabelPais;
    private javax.swing.JLabel jLabelProdutos;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JLabel jLabelRazaoSocial;
    private javax.swing.JLabel jLabelTelefone1;
    private javax.swing.JLabel jLabelTelefone2;
    private javax.swing.JLabel jLabelTipoPessoa;
    private javax.swing.JLabel jLabelValorCompra;
    private javax.swing.JLabel jLabelValorVenda;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableFornecedores;
    private javax.swing.JTable jTableProdutos;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldDescricao;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldTelefone1;
    private javax.swing.JTextField jTextFieldTelefone2;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables
}
